/*
***************** INFO ******************
Linux compile: mpic++ DotProduct.cpp -o DotProduct
Linux run: mpirun -n 4 ./DotProduct
Windows run: mpiexec -n 4 ./DotProduct.exe
*****************************************

**************** WARNING ****************
1. Use "-n" greater than 1
2. Check "bufferSize" value
*****************************************
*/
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <mpi.h>
#include <fstream>
#include <string.h>

using namespace std;

int main(int argc, char* argv[])
{
    int rank;
    int size;
    int bufferSize = 2; //check value
    double sum = 0;
    double value = 0;
    int iter;
//wczytywanie z plikow generowanych przez program Random_vector
    string valueX_temp;
    vector <double> tabX;
    ifstream myFile_1("vector_1.csv");
    while(getline(myFile_1, valueX_temp) )
    {
    		double valueX;
    		valueX=atof(valueX_temp.c_str());
		tabX.push_back(valueX);
		getline(myFile_1,valueX_temp);
    }
    string valueY_temp;
    double valueY;
    vector <double> tabY;
    ifstream myFile_2("vector_2.csv");
    while(getline(myFile_2, valueY_temp) )
    {
    		double valueY;
    		valueY=atof(valueY_temp.c_str());
		tabY.push_back(valueY);
		getline(myFile_2,valueY_temp);
    }
    
//    vector < double > tabX{ 1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.1, 8.2, 9.3, 10.4, 11.5, 12.6, 13.7, 14.8, 15.9 }; //first vector
//    vector < double > tabY{ 1.9, 2.8, 3.7, 4.6, 5.5, 6.4, 7.9, 8.8, 9.7, 10.6, 11.5, 12.4, 13.3, 14.2, 15.1 }; //second vector

//inicjalizacja MPI
    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

//wyznaczenie liczby operacji mnożenia przypadającej na każdy rdzeń procesora
    if ((tabX.size() % (size - 1)) == 0) {
        iter = ((tabX.size()) / (size - 1));
    }
//uzupełnianie wektorów zerami i jedynkami, żeby każdy rdzeń procesora wykonywał taką samą liczbę operacji mnożenia    
    else
    {
        iter = ((tabX.size()) / (size - 1)) + 1;
        for (int i = 1; i <= (iter * size - tabX.size()); i++) {
            tabX.push_back(1);
            tabY.push_back(0);
        }
    }

//obliczenia iloczynu
    if (rank != 0) {
        for (int j = 1; j <= iter; j++) {
            int k = iter * (rank - 1) + (j - 1); //numer wiersza wektora "tabX" oraz "tabY"
            value = value + tabX[k] * tabY[k];
        }
        MPI_Send(&value, 1, MPI_DOUBLE, 0, rank, MPI_COMM_WORLD); //wysyłanie wartości lokalnych
    }
//odbieranie wartości lokalnych, sumowanie i wyświetlenie wyników
    else if (rank == 0) {
        for (int i = 1; i < size; i++) {
            MPI_Recv(&value, bufferSize, MPI_DOUBLE, MPI_ANY_SOURCE, i, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            printf("P: %d locValue:  %f\n", i, value);
            sum = sum + value;
        }
        printf("P: %d result:    %f\n", rank, sum);
    }
	
    MPI_Finalize();
    return 0;
}
