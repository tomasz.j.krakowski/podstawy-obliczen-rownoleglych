﻿/*
***************** INFO ******************
Linux compile: mpic++ DotProduct.cpp -o DotProduct
Linux run: mpirun -n 4 ./DotProduct
Windows run: mpiexec -n 4 ./DotProduct.exe
*****************************************

**************** WARNING ****************
1. Use "-n" greater than 1
2. Check "bufferSize" value
*****************************************
*/
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
//#include <mpi.h>
#include <fstream>
#include <string.h>

using namespace std;

int main(int argc, char* argv[])
{
    int rank;
    int size;
    int bufferSize = 2; //check value
    double sum = 0;
    double value = 0;
    int iter;
//wczytywanie z plikow generowanych przez program Random_vector
    string valueX_temp;
    vector <double> tabX;
    ifstream myFile_1("vector_1.csv");
    while(getline(myFile_1, valueX_temp) )
    {
    		double valueX;
    		valueX=atof(valueX_temp.c_str());
		tabX.push_back(valueX);
		getline(myFile_1,valueX_temp);
    }
    string valueY_temp;
    double valueY;
    vector <double> tabY;
    ifstream myFile_2("vector_2.csv");
    while(getline(myFile_2, valueY_temp) )
    {
    		double valueY;
    		valueY=atof(valueY_temp.c_str());
		tabY.push_back(valueY);
		getline(myFile_2,valueY_temp);
    }
    
//    vector < double > tabX{ 1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.1, 8.2, 9.3, 10.4, 11.5, 12.6, 13.7, 14.8, 15.9 }; //first vector
//    vector < double > tabY{ 1.9, 2.8, 3.7, 4.6, 5.5, 6.4, 7.9, 8.8, 9.7, 10.6, 11.5, 12.4, 13.3, 14.2, 15.1 }; //second vector
//okreslenie wielkosci wektorow wejsciowych
    iter = tabX.size();
    
//mnozenie weektorow 
    for (int j = 0; j <= iter; j++)
       {
            value = value + tabX[j] * tabY[j];
       }
//wyswietlanie wynikow
   printf("Result:    %f\n", value);
   printf("\n");
}
