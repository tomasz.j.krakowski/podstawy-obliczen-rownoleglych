#include <random>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <functional>
#include <fstream>

using namespace std;

int main()
{
   	cout <<"Podaj dlugosc wektorow: \n";
   	int vector_len;
   	cin >> vector_len;
// Incijalizacja silnika randomizujacego
   	 random_device rnd_device;
// Okreslenie silnika i dystrybucji liczb pseudolosowych
    	mt19937 mersenne_engine {rnd_device()};
    	student_t_distribution<double> dist {0.98};
   	
    	auto gen = [&dist, &mersenne_engine]()
    		{
                   return dist(mersenne_engine);
		};
	auto gen_1 = [&dist, &mersenne_engine]()
    		{
                   return dist(mersenne_engine);
		};
//Wektor 1
    	vector<double> vec(vector_len);
    	generate(begin(vec), end(vec), gen);
//Wektor 2
    	vector<double> vec_1(vector_len);
    	generate(begin(vec_1), end(vec_1), gen_1);
//Wypisanie wektora w konsoli
        //Debug
   	//for (auto i : vec) 
   	//{
        //cout << i << "\n";
    	//}
//zapis do plikow csv
    	ofstream myFile("vector_1.csv");
	int vsize = vec.size();
	for (int n=0; n<vsize; n++)
	{
    		myFile << vec[n] << endl;
	}
	myFile.close();
	
	ofstream myFile_1("vector_2.csv");
	int vsize_1 = vec_1.size();
	for (int n=0; n<vsize_1; n++)
	{
    		myFile_1 << vec_1[n] << endl;
	}
	myFile_1.close();
    
    return 0;

}

