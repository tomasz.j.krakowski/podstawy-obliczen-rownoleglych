/*
***************** INFO ******************
Linux compile: mpic++ DotProduct.cpp -o DotProduct
Linux run: mpirun -n 4 ./DotProduct
Windows run: mpiexec -n 4 ./DotProduct.exe
*****************************************

**************** WARNING ****************
1. Use "-n" greater than 1
*****************************************
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <mpi.h>
#include <fstream>
#include <string>
#include <string.h>

using namespace std;

int main(int argc, char* argv[])
{
    int rank;
    int size;
    double sum = 0;
    double value = 0;
    int iter;

//wczytywanie z plikow generowanych przez program Random_vector
    string valueX_temp;
    vector <double> tabX;
    ifstream myFile_1("vector_1.csv");
    while (getline(myFile_1, valueX_temp))
    {
        double valueX;
        valueX = atof(valueX_temp.c_str());
        tabX.push_back(valueX);
        getline(myFile_1, valueX_temp);
    }
    string valueY_temp;
    double valueY;
    vector <double> tabY;
    ifstream myFile_2("vector_2.csv");
    while (getline(myFile_2, valueY_temp))
    {
        double valueY;
        valueY = atof(valueY_temp.c_str());
        tabY.push_back(valueY);
        getline(myFile_2, valueY_temp);
    }

 //   vector < double > tabX{ 1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9 }; //first vector
 //   vector < double > tabY{ 1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9 }; //second vector

//inicjalizacja MPI
    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

//wyznaczenie liczby operacji mnożenia przypadającej na każdy rdzeń procesora
    if ((tabX.size() % (size - 1)) == 0) {
        iter = ((tabX.size()) / (size - 1));
    }

//uzupełnianie wektorów zerami i jedynkami, żeby każdy rdzeń procesora wykonywał taką samą liczbę operacji mnożenia
    else
    {
        iter = ((tabX.size()) / (size - 1)) + 1;
        for (int i = 1; i <= (iter * size - tabX.size()); i++) {
            tabX.push_back(1);
            tabY.push_back(0);
        }
    }
        
//obliczenia iloczynu
    if (rank != 0) {
        for (int j = 1; j <= iter; j++) {
            int k = iter * (rank - 1) + (j - 1);  //numer wiersza wektora "tabX" oraz "tabY"
            value = value + tabX[k] * tabY[k];
        }
        //printf("P: %d locValue:  %f\n", rank, value);
    }
//zebranie i zsumowanie wartości lokalnych przez procesor 0
    MPI_Reduce(&value, &sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
//wyswietlenie wynikow
    if (rank == 0) {
        printf("P: %d result:    %f\n",  rank, sum);
    }

    MPI_Finalize();
    return 0;
}
