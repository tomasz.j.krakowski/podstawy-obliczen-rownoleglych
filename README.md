## Podstawy Obliczeń Równoległych - projekt zaliczeniowy
Repozytorium zawiera projekt zaliczeniowy z przedmiotu Podstawy Obliczeń Równoległych. 

Projekt został wykonany przez Krzysztofa Plewę (132868)  i Tomasza Krakowskiego (132814).

Celem projektu jest opracowanie programu równoległego do wyznaczania iloczynu skalarnego 2 wektorów. Urównoleglenie przeprowadzono z wykorzystaniem protokołu MPI. Przeprowadzono sprawdzenie czasu obliczeń w zależności od liczby wykorzystanych rdzeni procesora.

## Uruchomienie

W celu automatyzacji przygotowano skrypty przyspieszające kompilację i uruchamianie testów.
Po sklonowaniu repozytorium należy przeprowadzić kompilację kodów źródłowych komendą

```sh
./Allrecompile
```
Następnie można uruchomić program w wersji podstawowej (4 rdzenie)
```sh
./Allrun
```
lub w wersji do testów wielowątkowości
```sh
./Allrun_multi
```
## Wyniki, wariant Recive (DotProduct.cpp)

Pierwszą serię testów przeprowadzono dla wektorów o długości 50000000 składający się ze wielkości typu double, co przekłada się na pliki wsadowe o wielkość 460MB.

| Liczba rdzeni | Czas [s] |
| ------ | ------ |
| 1 | 7.34 |
| 4 | 30.24 |
| 8 | 61.73 |
| 12 | 92.93 |
## Wyniki, wariant Reduce (DotProduct_ver2.cpp)
W celu poprawy pierwszej wersji programu bazującej na prostej implementacji MPI_send i MPI_recive, wykonano kolejną wersję w której zastąpiono MPI_recive, poleceniem MPI_Reduce i operacją sumowania. Dla wektora o długość 100000 (0.9MB) uzyskano następujące wyniki:

| Liczba rdzeni | Czas [s] |
| ------ | ------ |
| 1 | 0.330 |
| 2 | 0.550 |
| 3 | 0.642 |
| 4 | 0.734 |
| 5 | 0.863 |
| 6 | 0.956 |

## Wnioski
Wariant szeregowy i równoległy pozwalają na uzyskanie takich samych wartości iloczynu skalarnego wektorów. Jednak nawet po wprowadzeniu poprawek w kodzie nie udało się uzyskać przyspieszenia wykonywania obliczeń, poprzez zastosowanie obliczeń równoległych. Wynika to z faktu czasu procesorowego poświęconego na podzielenie danych na pod-wektory, które są następnie wysyłane do poszczególnych wątków, a także czasu potrzebnego na sumowanie wyniku końcowego. 

Lepsze efekty dla danej konfiguracji (komputer stacjonarny - 1 procesor wielowątkowy) i dla danego problemu obliczeniowego można uzyskać przy zastosowaniu biblioteki OpenMP. Ze względu na przystosowanie do pracy w systemach z pamięcią współdzieloną.

Założony cel projektu został osiągnięty, wykonano udane urównoleglenie iloczynu skalarnego wektorów.
